# README

## Introduction

Ce dépôt contient plusieurs projets et exercices techniques réalisés dans le cadre de tests pour des entretiens d'embauche. Chaque projet est organisé dans un répertoire distinct et est accompagné de son propre ensemble d'instructions, de code source et de documentation.
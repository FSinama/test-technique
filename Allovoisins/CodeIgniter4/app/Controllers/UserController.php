<?php

namespace App\Controllers;

use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use ReflectionException;

class UserController extends ResourceController
{
    protected $modelName = 'App\Models\UserModel';
    protected $format = 'json';

    public function index()
    {
        return view('welcome_message');
    }

    public function list(): ResponseInterface
    {
        try {
            $page = $this->request->getGet('page') ?? 1;
            $limit = $this->request->getGet('limit') ?? 10;
        } catch (ReflectionException $e) {
            return $this->fail($e->getMessage());
        }
        return $this->respond($this->model->getUsers($page, $limit), 200, 'Utilisateurs récupérés avec succès');
    }

    public function show($id = null)
    {
        try {
            $user = $this->model->find($id);
        } catch (ReflectionException $e) {
            return $this->fail($e->getMessage());
        }
        return $this->respond($user, 200, 'Utilisateur récupéré avec succès');
    }

    public function create()
    {
        $data = $this->request->getBody();
        $data = json_decode($data, true);

        if (empty($data)) {
            return $this->fail('Aucune donnée n\'est fournie pour la création d\'un utilisateur');
        }

        try {
            $userId = $this->model->createUser($data);
        } catch (ReflectionException $e) {
            return $this->fail($e->getMessage());
        }

        if ($userId === false) {
            return $this->fail($this->model->errors());
        }

        return $this->respondCreated(message: 'Utilisateur créé avec succès');
    }

    public function update($id = null)
    {
        $data = $this->request->getBody();
        $data = json_decode($data, true);

        if (empty($data)) {
            return $this->response->setStatusCode(400)->setJSON(['message' => 'Les données de mise à jour sont vides.']);
        }

        try {
            $updated = $this->model->updateUser($id, $data);
        } catch (ReflectionException $e) {
            return $this->fail($e->getMessage());
        }

        if ($updated === false) {
            return $this->fail($this->model->errors());
        }

        return $this->respondUpdated(message: 'Utilisateur mis à jour avec succès');
    }

    public function delete($id = null)
    {
        try {
            $this->model->deleteUser($id);
        } catch (ReflectionException $e) {
            return $this->fail($e->getMessage());
        }
        return $this->respondDeleted(message: 'Utilisateur supprimé avec succès');
    }
}
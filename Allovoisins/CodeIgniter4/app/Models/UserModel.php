<?php

namespace App\Models;

use CodeIgniter\Database\BaseResult;
use CodeIgniter\Database\Query;
use CodeIgniter\Model;
use Config\Services;
use Exception;
use ReflectionException;

class UserModel extends Model
{
    protected $table            = 'users';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'first_name',
        'last_name',
        'email',
        'phone_number',
        'postal_address',
        'professional_status',
        'last_login'
    ];

    protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    protected array $casts = [];
    protected array $castHandlers = [];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [
        'first_name' => 'required|alpha_space|min_length[2]',
        'last_name' => 'required|alpha_space|min_length[2]',
        'email' => 'required|valid_email|is_unique[users.email]',
        'phone_number' => 'required|numeric',
        'postal_address' => 'required',
        'professional_status' => 'required',
        'last_login' => 'required|valid_date'
    ];
    protected $validationMessages   = [
        'first_name' => [
            'required' => 'Le prénom est obligatoire.',
            'alpha_space' => 'Le prénom ne doit contenir que des lettres et des espaces.',
            'min_length' => 'Le prénom doit contenir au moins 2 caractères.'
        ],
        'last_name' => [
            'required' => 'Le nom est obligatoire.',
            'alpha_space' => 'Le nom ne doit contenir que des lettres et des espaces.',
            'min_length' => 'Le nom doit contenir au moins 2 caractères.'
        ],
        'email' => [
            'required' => 'L\'adresse e-mail est obligatoire.',
            'valid_email' => 'L\'adresse e-mail n\'est pas valide.',
            'is_unique' => 'L\'adresse e-mail existe déjà.'
        ],
        'phone_number' => [
            'required' => 'Le numéro de téléphone est obligatoire.',
            'numeric' => 'Le numéro de téléphone ne doit contenir que des chiffres.'
        ],
        'postal_address' => [
            'required' => 'L\'adresse postale est obligatoire.'
        ],
        'professional_status' => [
            'required' => 'Le statut professionnel est obligatoire.'
        ],
        'last_login' => [
            'required' => 'La date de dernière connexion est obligatoire.',
            'valid_date' => 'La date de dernière connexion n\'est pas valide.'
        ]
    ];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function getUsers($page, $limit): ?array
    {
        $pager = Services::pager();
        $users = $this->paginate($limit, 'default', $page);
        $pagination = [
            'current_page' => $pager->getCurrentPage('default'),
            'total_pages' => $pager->getPageCount(),
            'total_users' => $pager->getTotal(),
        ];
        return [
            'data' => $users,
            'pagination' => $pagination
        ];
    }

    /**
     * @throws ReflectionException
     * @throws Exception
     */
    public function createUser($data): Query|bool|BaseResult
    {
        return $this->db->table($this->table)->insert($data);
    }

    public function updateUser($id, $data): bool
    {
        return $this->db->table($this->table)->where('id', $id)->update($data);
    }

    public function deleteUser($id): bool|string
    {
        return $this->db->table($this->table)->where('id', $id)->delete();
    }

    public function getInactiveUsers(int $months)
    {
        $date = date('Y-m-d H:i:s', strtotime("-$months months"));
        return $this->where('last_login <', $date)->findAll();
    }
}
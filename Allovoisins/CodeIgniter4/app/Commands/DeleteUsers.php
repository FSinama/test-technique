<?php

namespace App\Commands;

use App\Models\UserModel;
use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\CLI;

class DeleteUsers extends BaseCommand
{
    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'Tasks';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'tasks:users:delete';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'Supprime les utilisateurs.';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = 'tasks:users:delete [arguments] [options]';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [
        'id' => 'Les ID des utilisateurs à supprimer, séparés par des espaces.'
    ];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [
        'force' => 'Forcer l\'exécution de l\'opération lorsqu\'elle est en production.',
        'dry-run' => 'Exécuter l\'opération sans supprimer d\'utilisateur.',
        'no-interaction' => 'Ne posez pas de questions interactives.',
        'inactive-since' => 'Nombre de mois écoulés depuis la dernière connexion pour considérer un utilisateur comme inactif. Valeur par défaut : 36.',
    ];

    /**
     * Actually execute a command.
     *
     * @param array $params
     */
    public function run(array $params): void
    {
        $ids = array_diff_assoc($params, CLI::getOptions());

        $force = key_exists('force',$params) ?? false;

        if (!$force && ENVIRONMENT === 'production') {
            CLI::write('Cette commande ne peut pas être exécutée en environnement de production sans l\'option --force.', 'red');
            return;
        }

        $dryRun = key_exists('dry-run',$params) ?? false;
        $noInteraction = key_exists('no-interaction',$params) ?? false;
        $inactiveSince = $params['inactive-since'] ?? 36;

        $userModel = new UserModel();

        $users = empty($ids) ? $userModel->getInactiveUsers($inactiveSince) : $userModel->find($ids);

        if (empty($users)) {
            CLI::write('Aucun utilisateur à supprimer.', 'yellow');
            return;
        }

        foreach ($users as $user) {

            if ($dryRun) {
                CLI::write('L\'utilisateur ' . $user['id'] . ' serait supprimé.', 'yellow');
                continue;
            }

            if (!$noInteraction && CLI::prompt('Voulez-vous vraiment supprimer l\'utilisateur ' . $user['id'] . ' ?', ['n', 'y']) === 'n') {
                continue;
            }

            if (!$userModel->delete($user['id'])) {
                CLI::write('Impossible de supprimer l\'utilisateur ' . $user['id'] . '.', 'red');
                continue;
            }

            CLI::write('L\'utilisateur ' . $user['id'] . ' a été supprimé.', 'green');
        }
    }
}

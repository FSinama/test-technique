<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        $faker = Factory::create();
        for ($i = 0; $i < 500; $i++) {
            $data = [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->unique()->safeEmail,
                'phone_number' => $faker->phoneNumber,
                'postal_address' => $faker->address,
                'professional_status' => $faker->jobTitle,
                'last_login' => $faker->dateTimeBetween('-36 months', 'now')->format('Y-m-d H:i:s'),
            ];
            $this->db->table('users')->insert($data);
        }
    }
}
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bienvenue sur l'API AlloVoisins</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: flex-start;
            height: 100vh;
        }

        .container {
            text-align: center;
            background-color: #fff;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            border-radius: 10px;
            width: 90%;
            max-width: 1200px;
            margin: 20px;
            overflow-y: auto;
            height: 95vh;
        }

        h1 {
            color: #333;
        }

        p {
            color: #666;
        }

        .route-list {
            list-style-type: none;
            padding: 0;
        }

        .route-list li {
            margin: 10px 0;
        }

        .route-list a {
            color: #007BFF;
            text-decoration: none;
        }

        .route-list a:hover {
            text-decoration: underline;
        }

        .swagger-section {
            text-align: left;
            margin-top: 20px;
        }

        .swagger-section h2, .swagger-section h3 {
            color: #333;
        }

        .swagger-section pre {
            background: #f8f8f8;
            padding: 10px;
            border-radius: 5px;
            overflow: auto;
            white-space: pre-wrap;
            word-wrap: break-word;
        }

        .route-block {
            margin-bottom: 30px;
            border-radius: 5px;
            border: 3px solid #ccc;
            padding: 20px;
            cursor: pointer;
        }

        .route-block.get {
            border-color: #28a745;
        }

        .route-block.post {
            border-color: #007bff;
        }

        .route-block.put {
            border-color: #ffc107;
        }

        .route-block.delete {
            border-color: #dc3545;
        }

        .route-block.collapsed .route-content {
            display: none;
        }

        .route-block.collapsed h3::after {
            font-weight: normal;
            color: #007bff;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Bienvenue sur l'API AlloVoisins</h1>
    <p>Utilisez les routes ci-dessous pour interagir avec l'API.</p>
    <ul class="route-list">
        <li><a href="/api/back/users" target="_blank">Liste des utilisateurs (Private)</a></li>
        <li><a href="/api/back/users/1" target="_blank">Détails de l'utilisateur (Private)</a></li>
        <li><a href="/api/back/users" target="_blank">Créer un utilisateur (Private)</a></li>
        <li><a href="/api/back/users/1" target="_blank">Mettre à jour un utilisateur (Private)</a></li>
        <li><a href="/api/back/users/1" target="_blank">Supprimer un utilisateur (Private)</a></li>
        <li><a href="/api/front/users" target="_blank">Créer un utilisateur (Public)</a></li>
        <li><a href="/api/front/users/1" target="_blank">Mettre à jour un utilisateur (Public)</a></li>
    </ul>
    <div class="swagger-section">
        <h2>Documentation des Routes</h2>
        <div class="route-block get collapsed">
            <h3>GET /api/back/users : Liste des utilisateurs (Private)</h3>
            <div class="route-content">
                <b>Response Success:</b>
                <pre>
{
  "data": [
    {
      "id": (int),
      "first_name": (string),
      "last_name": (string),
      "email": (string),
      "phone_number": (string),
      "postal_address": (string),
      "professional_status": (string),
      "last_login": (date_time),
      "created_at": (date_time),
      "updated_at": (date_time)
    },
    ...
  ],
  "pagination": {
    "current_page": ...,
    "total_pages": ...,
    "total_users": ...
  }
}
                </pre>
                <b>Response Error:</b>
                <pre>
{
  "status": 400,
  "message": "Erreur lors de la récupération des utilisateurs"
}
                </pre>
            </div>
        </div>
        <div class="route-block get collapsed">
            <h3>GET /api/back/users/:id : Détails de l'utilisateur (Private)</h3>
            <div class="route-content">
                <b>Response Success:</b>
                <pre>
{
    "id": (int),
    "first_name": (string),
    "last_name": (string),
    "email": (string),
    "phone_number": (string),
    "postal_address": (string),
    "professional_status": (string),
    "last_login": (date_time),
    "created_at": (date_time),
    "updated_at": (date_time)
}
                </pre>
                <b>Response Error:</b>
                <pre>
{
  "status": 400,
  "message": "Erreur lors de la récupération de l'utilisateur"
}
                </pre>
            </div>
        </div>
        <div class="route-block post collapsed">
            <h3>POST /api/back/users : Créer un utilisateur (Private)</h3>
            <div class="route-content">
                <b>Request Body:</b>
                <pre>
{
  "first_name": (string),
  "last_name": (string),
  "email": (string),
  "phone_number": (string),
  "postal_address": (string),
  "professional_status": (string),
  "last_login": (date_time)
}
                </pre>
                <b>Response Success:</b>
                <pre>
{
  "id": (int)
}
                </pre>
                <b>Response Error:</b>
                <pre>
{
  "status": 400,
  "message": "Erreur lors de la création de l'utilisateur"
}
                </pre>
            </div>
        </div>
        <div class="route-block put collapsed">
            <h3>PUT /api/back/users/:id : Mettre à jour un utilisateur (Private)</h3>
            <div class="route-content">
                <b>Request Body:</b>
                <pre>
{
    "first_name": (string),
    "last_name": (string),
    "email": (string),
    "phone_number": (string),
    "postal_address": (string),
    "professional_status": (string),
    "last_login": (date_time)
}
                </pre>
                <b>Response Success:</b>
                <pre>
{
  "status": 200,
  "message": "Utilisateur mis à jour avec succès"
}
                </pre>
                <b>Response Error:</b>
                <pre>
{
  "status": 400,
  "message": "Erreur lors de la mise à jour de l'utilisateur"
}
                </pre>
            </div>
        </div>
        <div class="route-block delete collapsed">
            <h3>DELETE /api/back/users/:id : Supprimer un utilisateur (Private)</h3>
            <div class="route-content">
                <b>Response Success:</b>
                <pre>
{
  "status": 200,
  "message": "Utilisateur supprimé avec succès"
}
                </pre>
                <b>Response Error:</b>
                <pre>
{
  "status": 400,
  "message": "Erreur lors de la suppression de l'utilisateur"
}
                </pre>
            </div>
        </div>
        <div class="route-block post collapsed">
            <h3>POST /api/front/users : Créer un utilisateur (Public)</h3>
            <div class="route-content">
                <b>Request Body:</b>
                <pre>
{
  "first_name": (string),
  "last_name": (string),
  "phone_number": (string),
  "postal_address": (string),
  "professional_status": (string),
  "last_login": (date_time)
}
                </pre>
                <b>Response Success:</b>
                <pre>
{
  "id": (int)
}
                </pre>
                <b>Response Error:</b>
                <pre>
{
  "status": 400,
  "message": "Erreur lors de la création de l'utilisateur"
}
                </pre>
            </div>
        </div>
        <div class="route-block put collapsed">
            <h3>PUT /api/front/users/:id : Mettre à jour un utilisateur (Public)</h3>
            <div class="route-content">
                <b>Request Body:</b>
                <pre>
{
    "first_name": (string),
    "last_name": (string),
    "phone_number": (string),
    "postal_address": (string),
    "professional_status": (string),
    "last_login": (date_time)
}
                </pre>
                <b>Response Success:</b>
                <pre>
{
  "id": (int)
}
                </pre>
                <b>Response Error:</b>
                <pre>
{
  "status": 400,
  "message": "Erreur lors de la mise à jour de l'utilisateur"
}
                </pre>
            </div>
        </div>
    </div>
</div>
<script>
    document.querySelectorAll('.route-block').forEach(block => {
        block.addEventListener('click', () => {
            block.classList.toggle('collapsed');
        });
    });
</script>
</body>
</html>

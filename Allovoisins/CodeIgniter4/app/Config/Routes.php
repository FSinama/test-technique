<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->group('api', function($routes)
{
    // Public routes
    $routes->group('front', function($routes)
    {
        $routes->post('users', 'UserController::create');
        $routes->put('users/(:segment)', 'UserController::update/$1');
    });

    // Private routes
    $routes->group('back', function($routes)
    {
        $routes->get('users', 'UserController::list');
        $routes->get('users/(:segment)', 'UserController::show/$1');
        $routes->post('users', 'UserController::create');
        $routes->put('users/(:segment)', 'UserController::update/$1');
        $routes->delete('users/(:segment)', 'UserController::delete/$1');
    });
});
$routes->get('/', 'UserController::index');

# API AlloVoisins - Test Technique

## Contexte
Ce projet a été développé dans le cadre d'un test technique pour un entretien d'embauche chez AlloVoisins. L'objectif est de créer une série d'API REST permettant de gérer des utilisateurs, avec les fonctionnalités suivantes :

- Récupération d’une liste paginée d’utilisateurs
- Création d’un utilisateur
- Modification d’un utilisateur
- Suppression d’un utilisateur

## Installation

1. Clonez le dépôt
2. Installez les dépendances via Composer :
   ```sh
   composer install
   ```
3. Configurez votre base de données dans `.env`
4. Exécutez les migrations pour créer les tables nécessaires :
   ```sh
   php spark migrate
   ```
5. Démarrez le serveur :
   ```sh
   php spark serve
   ```

## Liste des routes

- `GET /api/back/users` : Liste des utilisateurs (Private)
- `GET /api/back/users/:id` : Détails de l'utilisateur (Private)
- `POST /api/back/users` : Créer un utilisateur (Private)
- `PUT /api/back/users/:id` : Mettre à jour un utilisateur (Private)
- `DELETE /api/back/users/:id` : Supprimer un utilisateur (Private)
- `POST /api/front/users` : Créer un utilisateur (Public)
- `PUT /api/front/users/:id` : Mettre à jour un utilisateur (Public)

## Documentation des routes

### GET /api/back/users

Récupère la liste des utilisateurs.

**Request Body**: Aucun

**Response Success**:
```json
{
  "data": [
    {
      "id": (int),
      "first_name": (string),
      "last_name": (string),
      "email": (string),
      "phone_number": (string),
      "postal_address": (string),
      "professional_status": (string),
      "last_login": (date_time),
      "created_at": (date_time),
      "updated_at": (date_time)
    },
    ...
  ],
  "pagination": {
    "current_page": ...,
    "total_pages": ...,
    "total_users": ...
  }
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la récupération des utilisateurs"
}
```

### GET /api/back/users/:id

Récupère les détails d'un utilisateur spécifique.

**Request Body**: Aucun

**Response Success**:
```json
{
    "id": (int),
    "first_name": (string),
    "last_name": (string),
    "email": (string),
    "phone_number": (string),
    "postal_address": (string),
    "professional_status": (string),
    "last_login": (date_time),
    "created_at": (date_time),
    "updated_at": (date_time)
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la récupération de l'utilisateur"
}
```

### POST /api/back/users

Crée un nouvel utilisateur.

**Request Body**:
```json
{
  "first_name": (string),
  "last_name": (string),
  "email": (string),
  "phone_number": (string),
  "postal_address": (string),
  "professional_status": (string),
  "last_login": (date_time)
}
```
**Response Success**:
```json
{
  "id": (int)
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la création de l'utilisateur"
}
```

### PUT /api/back/users/:id

Mettre à jour un utilisateur existant.

**Request Body**:
```json
{
    "first_name": (string),
    "last_name": (string),
    "email": (string),
    "phone_number": (string),
    "postal_address": (string),
    "professional_status": (string),
    "last_login": (date_time)
}
```
**Response Success**:
```json
{
  "status": 200,
  "message": "Utilisateur mis à jour avec succès"
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la mise à jour de l'utilisateur"
}
```

### DELETE /api/back/users/:id

Supprime un utilisateur existant.

**Request Body**: Aucun

**Response Success**:
```json
{
  "status": 200,
  "message": "Utilisateur supprimé avec succès"
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la suppression de l'utilisateur"
}
```

### POST /api/front/users

Crée un nouvel utilisateur.

**Request Body**:
```json
{
  "first_name": (string),
  "last_name": (string),
  "phone_number": (string),
  "postal_address": (string),
  "professional_status": (string),
  "last_login": (date_time)
}
```
**Response Success**:
```json
{
  "id": (int)
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la création de l'utilisateur"
}
```

### PUT /api/front/users/:id

Mettre à jour un utilisateur existant.

**Request Body**:
```json
{
    "first_name": (string),
    "last_name": (string),
    "phone_number": (string),
    "postal_address": (string),
    "professional_status": (string),
    "last_login": (date_time)
}
```
**Response Success**:
```json
{
  "id": (int)
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la mise à jour de l'utilisateur"
}
```

## Tâche Cron

La commande `DeleteUsers` est une commande CLI (Command Line Interface) qui permet de supprimer des utilisateurs. Elle est définie dans le fichier `app/Commands/DeleteUsers.php`.

La commande peut être exécutée avec plusieurs options et arguments :

- `id` : Les ID des utilisateurs à supprimer, séparés par des espaces.
- `--force` : Forcer l'exécution de l'opération lorsqu'elle est en production.
- `--dry-run` : Exécuter l'opération sans supprimer d'utilisateur.
- `--no-interaction` : Ne posez pas de questions interactives.
- `--inactive-since` : Nombre de mois écoulés depuis la dernière connexion pour considérer un utilisateur comme inactif. Valeur par défaut : 36.

La commande commence par récupérer les ID des utilisateurs à supprimer à partir des arguments passés. Si aucun ID n'est fourni, elle récupère les utilisateurs inactifs depuis un certain nombre de mois (par défaut 36 mois).

Ensuite, elle vérifie si l'option `--force` est activée. Si elle ne l'est pas et que l'application est en environnement de production, la commande s'arrête et affiche un message d'erreur.

Si l'option `--dry-run` est activée, la commande affiche les utilisateurs qui seraient supprimés mais ne les supprime pas réellement.

Si l'option `--no-interaction` n'est pas activée, la commande demande une confirmation avant de supprimer chaque utilisateur.

Enfin, la commande tente de supprimer chaque utilisateur et affiche un message indiquant si l'opération a réussi ou non.

Pour utiliser cette commande, vous pouvez l'exécuter à partir de la ligne de commande comme suit :

```bash
php spark tasks:users:delete --force --dry-run
```

Cela exécutera la commande en mode "dry run", ce qui signifie qu'aucun utilisateur ne sera réellement supprimé. Pour supprimer réellement les utilisateurs, vous pouvez supprimer l'option `--dry-run` :

```bash
php spark tasks:users:delete --force
```

Si vous voulez supprimer des utilisateurs spécifiques, vous pouvez passer leurs ID en argument :

```bash
php spark tasks:users:delete 1 2 3 --force
```

Cela supprimera les utilisateurs avec les ID 1, 2 et 3.

### Configuration de la tâche cron

Pour lancer la tâche cron, vous devez d'abord définir une entrée cron sur votre serveur. Cette entrée indiquera au serveur d'exécuter la commande à un intervalle spécifique.

Voici comment vous pouvez le faire :

1. Ouvrez le crontab avec la commande suivante :
```bash
crontab -e
```

2. Ajoutez une nouvelle ligne à la fin du fichier avec le format suivant :
```bash
0 0 * * * cd /chemin/vers/votre/projet && php spark tasks:users:delete --force
```
Remplacez `/chemin/vers/votre/projet` par le chemin absolu vers le répertoire de votre projet.

Cette commande sera exécutée tous les jours à minuit. Vous pouvez modifier le timing selon vos besoins.

3. Enregistrez et fermez le fichier. La nouvelle tâche cron est maintenant définie et sera exécutée à l'intervalle spécifié.

Notez que l'option `--force` est utilisée pour forcer l'exécution de la commande en environnement de production. Si vous ne voulez pas forcer l'exécution, vous pouvez supprimer cette option.

## Estimation du Temps Passé

- Configuration initiale et apprentissage : 2 heures
- Développement des routes et des contrôleurs : 4 heures
- Création des vues et documentation : 1 heures
- Total : 7 heures

## Conclusion

Ce projet démontre la capacité à développer une API RESTful avec CodeIgniter 4, à documenter les routes de manière claire et à implémenter des bonnes pratiques de développement.
<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Contrôleur API pour les utilisateurs.
 * @package controllers/api
 * @property User_model $userModel La classe User_model.
 */
class Users extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model', 'userModel');
		$this->load->helper('url');
		$this->load->library('form_validation');
	}

	/**
	 * Obtenir tous les utilisateurs.
	 */
	public function list(): void
	{
		$page = $this->input->get('page') ?: 1;
		$limit = $this->input->get('limit') ?: 10;
		$offset = ($page - 1) * $limit;

		$users = $this->userModel->get_users_paginated($limit, $offset);
		$total_users = $this->userModel->get_users_count();

		$response = [
			'data' => $users,
			'pagination' => [
				'current_page' => $page,
				'total_pages' => ceil($total_users / $limit),
				'total_users' => $total_users,
			]
		];

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function show(int $id): void
	{
		$user = $this->userModel->get_user($id);

		if (empty($user)) {
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode(['error' => 'Utilisateur non trouvé']));
			return;
		}

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($user));
	}

	/**
	 * Créer un nouvel utilisateur et retourner son ID.
	 */
	public function create(): void
	{
		$data = $this->get_valid_data();

		if ($data === false) {
			return;
		}

		$userId = $this->userModel->insert_user($data);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['id' => $userId]));
	}

	/**
	 * Mettre à jour un utilisateur existant.
	 *
	 * @param int $id L'ID de l'utilisateur à mettre à jour.
	 */
	public function update(int $id): void
	{
		$data = $this->get_valid_data();

		if ($data === false) {
			return;
		}

		$this->userModel->update_user($id, $data);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['status' => 'Mise à jour de l\'utilisateur réussie']));
	}

	/**
	 * Supprimer un utilisateur.
	 *
	 * @param int $id L'ID de l'utilisateur à supprimer.
	 */
	public function delete(int $id): void
	{
		$this->userModel->delete_user($id);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(['status' => 'Utilisateur supprimé avec succès']));
	}

	/**
	 * Définir des règles de validation pour les données de l'utilisateur.
	 */
	private function set_validation_rules(): void
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('phone_number', 'Phone', 'required');
		$this->form_validation->set_rules('postal_address', 'Address', 'required');
		$this->form_validation->set_rules('professional_status', 'Professional Status', 'required');
		$this->form_validation->set_message('required', 'Le champ {field} est obligatoire.');
		$this->form_validation->set_message('valid_email', 'Le champ {field} doit contenir une adresse email valide.');
		$this->form_validation->set_message('is_unique', 'L\'adresse e-mail existe déjà.');
	}

	/**
	 * Valider les données de la requête.
	 *
	 * @return array|bool Les données validées ou false si les données ne sont pas valides.
	 */
	public function get_valid_data(): array|bool
	{
		$data = json_decode($this->input->raw_input_stream, true);

		if (is_null($data)) {
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode(['error' => 'Format JSON non valide']));
			return false;
		}

		$this->form_validation->set_data($data);
		$this->set_validation_rules();

		if ($this->form_validation->run() === FALSE) {
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode(['error' => validation_errors()]));
			return false;
		}

		return $data;
	}
}

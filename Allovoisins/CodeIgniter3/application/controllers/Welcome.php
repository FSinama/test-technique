<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Contrôleur pour la page d'accueil.
 */
class Welcome extends CI_Controller {

	/**
	 * Affiche la page d'accueil.
	 */
	public function index(): void
	{
		$this->load->view('welcome_message');
	}
}

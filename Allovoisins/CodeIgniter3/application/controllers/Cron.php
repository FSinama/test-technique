<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Contrôleur pour les tâches cron.
 * @package controllers
 * @property User_model $userModel La classe User_model.
 */
class Cron extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('User_model', 'userModel');
	}

	/**
	 * Supprimer les utilisateurs inactifs depuis plus de 36 mois.
	 */
	public function remove_inactive_users(): void
	{
		$users = $this->userModel->get_inactive_users(34);

		foreach ($users as $user) {
			echo 'Suppression de l\'utilisateur ' . $user['id'] . PHP_EOL;
			$this->userModel->delete_user($user['id']);
		}
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class User_model
 *
 * This class interacts with the database to perform CRUD operations on the users table.
 */
class User_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_user(int $id): ?array
	{
		$query = $this->db->get_where('users', ['id' => $id]);
		return $query->row_array();
	}

	/**
	 * Get all users from the database with pagination.
	 *
	 * @param int $limit The number of users to retrieve.
	 * @param int $offset The offset for pagination.
	 * @return array An array of user data.
	 */
	public function get_users_paginated(int $limit, int $offset): array
	{
		$query = $this->db->get('users', $limit, $offset);
		return $query->result_array();
	}

	/**
	 * Get the total count of users in the database.
	 *
	 * @return int The total number of users.
	 */
	public function get_users_count(): int
	{
		return $this->db->count_all('users');
	}

	/**
	 * Insert a new user into the database.
	 *
	 * @param array $data The user data to insert.
	 * @return int The ID of the newly inserted user.
	 */
	public function insert_user(array $data): int
	{
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}

	/**
	 * Update an existing user in the database.
	 *
	 * @param int $id The ID of the user to update.
	 * @param array $data The user data to update.
	 * @return void
	 */
	public function update_user(int $id, array $data): void
	{
		$this->db->where('id', $id);
		$this->db->update('users', $data);
	}

	/**
	 * Delete a user from the database.
	 *
	 * @param int $id The ID of the user to delete.
	 * @return void
	 */
	public function delete_user(int $id): void
	{
		$this->db->where('id', $id);
		$this->db->delete('users');
	}

	public function get_inactive_users(int $months): array
	{
		$date = date('Y-m-d H:i:s', strtotime("-$months months"));
		$query = $this->db->get_where('users', ["last_login <" => $date]);
		return $query->result_array();
	}
}

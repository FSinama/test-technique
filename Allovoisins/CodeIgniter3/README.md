# Mon Projet

## Installation et démarrage du projet

1. Clonez le dépôt à partir de GitHub.
```bash
git clone https://gitlab.com/FSinama/allovoisins
```
2. Naviguez vers le répertoire du projet.
```bash
cd allovoisins
```
3. Installez les dépendances.
```bash
composer install
```
4. Configurez votre environnement dans application/config/database.php.
```bash
cp .env.example .env
```

## Liste des routes

- `GET /api/back/users` : Liste des utilisateurs (Private)
- `GET /api/back/users/:id` : Détails de l'utilisateur (Private)
- `POST /api/back/users` : Créer un utilisateur (Private)
- `PUT /api/back/users/:id` : Mettre à jour un utilisateur (Private)
- `DELETE /api/back/users/:id` : Supprimer un utilisateur (Private)
- `POST /api/front/users` : Créer un utilisateur (Public)
- `PUT /api/front/users/:id` : Mettre à jour un utilisateur (Public)

## Documentation des routes

### GET /api/back/users

Récupère la liste des utilisateurs.

**Request Body**: Aucun

**Response Success**:
```json
{
  "data": [
    {
      "id": (int),
      "first_name": (string),
      "last_name": (string),
      "email": (string),
      "phone_number": (string),
      "postal_address": (string),
      "professional_status": (string),
      "last_login": (date_time),
      "created_at": (date_time),
      "updated_at": (date_time)
    },
    ...
  ],
  "pagination": {
    "current_page": ...,
    "total_pages": ...,
    "total_users": ...
  }
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la récupération des utilisateurs"
}
```

### GET /api/back/users/:id

Récupère les détails d'un utilisateur spécifique.

**Request Body**: Aucun

**Response Success**:
```json
{
    "id": (int),
    "first_name": (string),
    "last_name": (string),
    "email": (string),
    "phone_number": (string),
    "postal_address": (string),
    "professional_status": (string),
    "last_login": (date_time),
    "created_at": (date_time),
    "updated_at": (date_time)
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la récupération de l'utilisateur"
}
```

### POST /api/back/users

Crée un nouvel utilisateur.

**Request Body**:
```json
{
  "first_name": (string),
  "last_name": (string),
  "email": (string),
  "phone_number": (string),
  "postal_address": (string),
  "professional_status": (string),
  "last_login": (date_time)
}
```
**Response Success**:
```json
{
  "id": (int)
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la création de l'utilisateur"
}
```

### PUT /api/back/users/:id

Mettre à jour un utilisateur existant.

**Request Body**:
```json
{
    "first_name": (string),
    "last_name": (string),
    "email": (string),
    "phone_number": (string),
    "postal_address": (string),
    "professional_status": (string),
    "last_login": (date_time)
}
```
**Response Success**:
```json
{
  "status": 200,
  "message": "Utilisateur mis à jour avec succès"
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la mise à jour de l'utilisateur"
}
```

### DELETE /api/back/users/:id

Supprime un utilisateur existant.

**Request Body**: Aucun

**Response Success**:
```json
{
  "status": 200,
  "message": "Utilisateur supprimé avec succès"
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la suppression de l'utilisateur"
}
```

### POST /api/front/users

Crée un nouvel utilisateur.

**Request Body**:
```json
{
  "first_name": (string),
  "last_name": (string),
  "phone_number": (string),
  "postal_address": (string),
  "professional_status": (string),
  "last_login": (date_time)
}
```
**Response Success**:
```json
{
  "id": (int)
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la création de l'utilisateur"
}
```

### PUT /api/front/users/:id

Mettre à jour un utilisateur existant.

**Request Body**:
```json
{
    "first_name": (string),
    "last_name": (string),
    "phone_number": (string),
    "postal_address": (string),
    "professional_status": (string),
    "last_login": (date_time)
}
```
**Response Success**:
```json
{
  "id": (int)
}
```
**Response Error**:
```json
{
  "status": 400,
  "message": "Erreur lors de la mise à jour de l'utilisateur"
}
```

## Tâche Cron

La tâche cron `remove_inactive_users` est configurée pour supprimer les utilisateurs inactifs depuis plus de 36 mois. Pour exécuter cette tâche, utilisez la commande suivante :

```bash
php index.php cron remove_inactive_users
```

Assurez-vous que cette tâche est configurée pour s'exécuter régulièrement selon vos besoins. Vous pouvez configurer cette tâche dans votre crontab en utilisant la commande `crontab -e` et en ajoutant une ligne comme celle-ci :

```bash
0 0 * * * /usr/bin/php /chemin/vers/votre/projet/index.php cron remove_inactive_users
```

Cette configuration exécutera la tâche cron tous les jours à minuit. Modifiez le timing selon vos besoins.


## Estimation du Temps Passé

- Configuration initiale et apprentissage : 2 heures
- Développement des routes et des contrôleurs : 4 heures
- Création des vues et documentation : 1 heures
- Total : 7 heures

## Conclusion

Ce projet démontre la capacité à développer une API RESTful avec CodeIgniter 3, à documenter les routes de manière claire et à implémenter des bonnes pratiques de développement.

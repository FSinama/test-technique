# Exercice Technique Back AlloVoisins

## 1. Objectif

Développer les API REST suivantes :

- Récupération d’une liste paginée d’utilisateurs (potentiellement 5M d’utilisateurs)
- Création d’un utilisateur
- Modification d’un utilisateur
- Suppression d’un utilisateur

### Informations Utilisateur

Un utilisateur comporte les informations suivantes :
- ID unique
- Prénom
- Nom
- Adresse email
- Numéro de téléphone
- Adresse postale
- Statut professionnel
- Date de dernière connexion

### Fonctionnalités des API

- **Inscription et modification** : Accessibles via des API publiques Front Office.
- **Listing, modification et suppression** : Accessibles via des API privées Back Office.
- **Batch/Cron** : Suppression régulière des utilisateurs inactifs (non connectés depuis 36 mois).

## 2. Contraintes

- **Technologies** : PHP8, CodeIgniter 3, MySQL
- **Modèle** : Utilisation du QueryBuilder de CI3
- **Qualité du Code** : Priorité à la qualité et aux bonnes pratiques

## 3. Rendu

- **Code** : Dépôt GIT ou archive ZIP. Une démonstration hébergée est un plus. Intégrer les requêtes SQL.
- **Documentation Technique** : Explication de l’utilisation de l’API et des choix de conception.
- **Estimation du Temps** : Détail du temps passé sur les différents aspects du projet.